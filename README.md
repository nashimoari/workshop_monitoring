# workshop_monitoring

## Grafana

http://localhost:30080/

Login/pass: admin/admin

## Prometheus

http://localhost:30081/


## Applications

- web_logs_to_db - It writes a data from web user activity to the DB.
- db_logs_to_kafka - It sends a data from the DB to the Kafka.
- kafka_aggregate_to_db - It receives a data from the Kafka and aggregate it 
and store at the DB.

## Links

## Postgres exporter
https://github.com/prometheus-community/postgres_exporter
https://grafana.com/grafana/dashboards/9628-postgresql-database/

## Kafka exporter
https://github.com/danielqsj/kafka_exporter
https://grafana.com/grafana/dashboards/7589-kafka-exporter-overview/


# SQL top queries

```sql

SELECT rolname,
    calls,
    total_exec_time,
    mean_exec_time,
    max_exec_time,
    stddev_exec_time,
    rows,
    query,
    regexp_replace(query, '[ \t\n]+', ' ', 'g') AS query_oneline
FROM pg_stat_statements
JOIN pg_roles r ON r.oid = userid
WHERE calls > 100
AND rolname NOT LIKE '%backup'
ORDER BY mean_exec_time DESC
LIMIT 15;

```