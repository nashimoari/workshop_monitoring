import requests


def reddit_oauth(username, password, client_id, secret_token):
    # note that CLIENT_ID refers to 'personal use script' and SECRET_TOKEN to 'token'
    auth = requests.auth.HTTPBasicAuth('<CLIENT_ID>', '<SECRET_TOKEN>')

    # here we pass our login method (password), username, and password
    data = {'grant_type': 'password',
            'username': '<USERNAME>',
            'password': '<PASSWORD>'}

    # setup our header info, which gives reddit a brief description of our app
    headers = {'User-Agent': 'MyBot/0.0.1'}

    # send our request for an OAuth token
    res = requests.post('https://www.reddit.com/api/v1/access_token',
                        auth=auth, data=data, headers=headers)

    if not res.ok:
        print(res.text)

    # convert response to JSON and pull access_token value
    access_token = res.json()['access_token']

    # add authorization to our headers dictionary
    headers = {**headers, **{'Authorization': f"bearer {TOKEN}"}}

    # while the token is valid (~2 hours) we just add headers=headers to our requests
    requests.get('https://oauth.reddit.com/api/v1/me', headers=headers)
    return access_token


def get_data():
    res = requests.get("https://oauth.reddit.com/r/python/hot", headers=headers)
