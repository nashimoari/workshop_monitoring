from kafka import KafkaProducer
from kafka import KafkaConsumer
import logging
import json


def producer_json_msg_builder(host: str, port: str):
    return KafkaProducer(bootstrap_servers=[f'{host}:{port}'],
                         value_serializer=lambda m: json
                         .dumps(m, indent=4, sort_keys=True, default=str)
                         .encode('ascii'),
                         retries=5)


def consumer_json_msg_builder(host: str, port: str, topic_name: str, grp_id: str):
    # To consume latest messages and auto-commit offsets
    return KafkaConsumer(topic_name,
                         group_id=grp_id,
                         bootstrap_servers=[f'{host}:{port}'],
                         value_deserializer=lambda m: json.loads(m.decode('ascii'))
                         )

    # consume json messages


    # consume earliest available messages, don't commit offsets
    # KafkaConsumer(auto_offset_reset='earliest', enable_auto_commit=False)

def produce_json_message(producer, topic_name: str, data_dict: dict):
    log = logging.getLogger("produce_message")

    def on_send_success(record_metadata):
        print(record_metadata.topic)
        print(record_metadata.partition)
        print(record_metadata.offset)

    def on_send_error(excp):
        log.error('I am an errback', exc_info=excp)
        # handle exception

    producer.send(topic_name, data_dict).add_callback(on_send_success).add_errback(on_send_error)


def consume_message(consumer):
    for message in consumer:
        # message value and key are raw bytes -- decode if necessary!
        # e.g., for unicode: `message.value.decode('utf-8')`
        print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                             message.offset, message.key,
                                             message.value))
