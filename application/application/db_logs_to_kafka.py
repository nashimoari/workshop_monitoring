import libs.kafka_connection as kaf
import libs.db_connection as db_conn
import time
import logging

log = logging.getLogger("db_logs_to_kafka")

producer = kaf.producer_json_msg_builder('kafka', '9092')

while True:
    # Receive new messages
    sql = """select * 
    from user_log u 
    where u.dt between date_trunc('min',now()) and date_trunc('min',now()) + '1 min'::interval"""
    data_list = db_conn.select_data_dict_with_commit(sql, {})

    # Send messages to the kafka
    for item in data_list:
        kaf.produce_json_message(producer, 'user_log', item)

    time.sleep(60)

