#!/usr/bin/env sh

python web_logs_to_db.py &
python db_logs_to_kafka.py &
python kafka_aggregate_to_db.py
