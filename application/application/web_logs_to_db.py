import libs.db_connection as db_conn
import random
import time

sql = """INSERT INTO user_log
SELECT generate_series(1, floor(random() * 100 + 1)::int),
floor(random() * 100000 + 1)::int,
floor(random() * 1000 + 1)::int,
case when floor(random() * 20 + 1)::int = 1 then 'click' else 'show' end;
"""

while True:
    db_conn.execute(sql)
    rnd = random.randint(0, 20)
    if rnd == 5:
        time.sleep(10)
    time.sleep(1)

