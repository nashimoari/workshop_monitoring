import libs.kafka_connection as kaf
import libs.db_connection as db_conn
import time
import logging
from collections import Counter

log = logging.getLogger("kafka_aggregate_to_db")

consumer = kaf.consumer_json_msg_builder('kafka', '9092', 'user_log', 'aggregates-to-db')

data = {'show': 0, 'click': 0}
ts = time.time()
while True:
    # Receive messages from kafka
    for message in consumer:
        # message value and key are raw bytes -- decode if necessary!
        # e.g., for unicode: `message.value.decode('utf-8')`
        # print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
        #                                      message.offset, message.key,
        #                                      message.value))

        if message.value['type'] == 'show':
            data['show'] += 1

        if message.value['type'] == 'click':
            data['click'] += 1

        if time.time() - ts >= 10:
            sql = "INSERT INTO aggregations (aggregation_code, agg_cnt) values ('show', %s), ('click', %s)"
            db_conn.execute_with_params(sql, [data['show'], data['click']])
            data = {'show': 0, 'click': 0}
            ts = time.time()
