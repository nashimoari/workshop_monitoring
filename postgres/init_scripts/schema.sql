/*
Задания по индексам
*/

-- sequences
create sequence seq_event_id;
create sequence seq_agg_id;

-- tables
create table user_log (
    event_id int
    ,user_id int
    ,category int
    ,type varchar
    ,dt timestamptz NOT NULL DEFAULT NOW()
    );


create table nomenclature (product_id int, product_name varchar);
create table product_details (product_id int, product_about varchar, product_specification varchar);

create table aggregations(
    agg_id int
    ,dt timestamptz NOT NULL DEFAULT NOW()
    ,aggregation_code varchar
    ,agg_cnt int
    );


INSERT INTO nomenclature SELECT generate_series(1,1000000), repeat('X', (random() * 100)::int);
INSERT INTO product_details SELECT generate_series(1,1000000), repeat('X', (random() * 100)::int), repeat('X', (random() * 100)::int);